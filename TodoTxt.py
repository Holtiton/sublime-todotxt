import sublime
import sublime_plugin

import os
import subprocess
import re
import datetime
from io import StringIO


DATE_REGEX = r'20\d{2}-[0-1]\d-[0-3]\d'
PRIORITY_REGEX = r'(?:\(([A-E])\) )'
TODO_REGEX = re.compile(r'^(x )?' + PRIORITY_REGEX + r'?('+ DATE_REGEX + r')?('+ DATE_REGEX + r')?(.*)$')

DATE_FORMAT = "%Y-%m-%d"

targetView = None
pending_output = None
event = None

def path_from_settings():
    settings = sublime.load_settings('TodoTxt.sublime-settings')
    return settings.get('todotxt_file')


class TodoTxtItem:
    def __init__(self):
        self.completed = False
        self.priority = None
        self.completion_date = None
        self.creation_date = None
        self.text = ""

    def parse(self, line):
        match = TODO_REGEX.match(line)
        if not match:
            return False

        self.completed = match.group(1) is not None
        self.priority = match.group(2)
        first_date_match = match.group(3)
        second_date_match = match.group(4)
        self.text = match.group(5)

        if first_date_match and second_date_match:
            if self.completed:
                self.completion_date = datetime.datetime.strptime(first_date_match, DATE_FORMAT)
                self.creation_date = datetime.datetime.strptime(second_date_match, DATE_FORMAT)
            else:
                return False
                print('Invalid task: Has completion date but is not marked complete')
        elif first_date_match and not second_date_match:
            self.creation_date = datetime.datetime.strptime(first_date_match, DATE_FORMAT)
        else:
            # shouldn't be possible to get the second match without the first as its
            # greedy
            return False

        return True

    def output(self):
        output = StringIO()
        if self.completed:
            output.write('x ')

        if self.priority:
            output.write('(')
            output.write(self.priority)
            output.write(') ')

        if self.completion_date and self.creation_date:
            output.write(self.completion_date.strftime(DATE_FORMAT))
            output.write(' ')
            output.write(self.creation_date.strftime(DATE_FORMAT))
        else:
            if self.creation_date:
                output.write(self.creation_date.strftime(DATE_FORMAT))
                output.write(' ')

        output.write(self.text)
        return output.getvalue()


class TodoTxtEventListener(sublime_plugin.EventListener):
    def on_load(self, view):
        global targetView
        global pending_output
        global event
        if view == targetView:
            if event == 'insert':
                view.run_command('todo_txt_insert_todo', {'output': pending_output})
                event = None
                targetView = None
                pending_output = None
            elif event == 'complete':
                view.run_command('todo_txt_move_to_todo', {'todo_item': pending_output})
                event = None
                targetView = None
                pending_output = None



class TodoLineInputHandler(sublime_plugin.TextInputHandler):
    def __init__(self, view):
        self.view = view

    def placeholder(self):
        return "Something needs to be done"


class TodoTxtAddTodoCommand(sublime_plugin.TextCommand):
    def run(self, edit, todo_line):
        todo = TodoTxtItem()
        todo.parse(todo_line)
        todo.creation_date = datetime.date.today()

        # I do not want to focus on the todo.txt file if it is already open
        # when adding new tasks which is why I use find_open_file.
        todo_view = self.view.window().find_open_file(self.todotxt_file_path)
        if todo_view:
            self.view.run_command('todo_txt_insert_todo', {'output':todo.output()})
        else:
            # If the file is not open I do want to pull focus to it which the
            # open_file command does automatically.
            todo_view = self.view.window().open_file(self.todotxt_file_path)
            if todo_view.is_loading():
                global targetView, pending_output, event
                event = 'insert'
                targetView = todo_view
                pending_output = todo.output()
            else:
                todo_view.run_command('todo_txt_insert_todo', {'output':todo.output()})

    def input(self, args):
        self.todotxt_file_path = path_from_settings()
        if self.todotxt_file_path is None or os.path.exists(self.todotxt_file_path) is False:
            # TODO: error out here as we do not have a file set
            return None
        else: 
            return TodoLineInputHandler(self.view)

    def is_visible(self):
        self.todotxt_file_path = path_from_settings()
        if self.todotxt_file_path is None or os.path.exists(self.todotxt_file_path) is False:
            return False
        else:
            return True


class TodoTxtInsertTodoCommand(sublime_plugin.TextCommand):
    def run(self, edit, output):
        self.todotxt_file_path = path_from_settings()
        todo_view = self.view.window().find_open_file(self.todotxt_file_path)

        todo_view.insert(edit, 0, output + '\n')


class TodoTxtCompleteSelectedTodoCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        edit_rows = []
        for region in self.view.sel():
            for line in self.view.lines(region):
                edit_rows.append(self.view.rowcol(line.begin())[0])
        
        for row in edit_rows:
            line = self.view.line(self.view.text_point(row, 0))
            todo = TodoTxtItem()
            todo.parse(self.view.substr(line))
            if not todo.completed:
                if todo.creation_date:
                    todo.completion_date = datetime.date.today()
                todo.completed = True
                self.view.replace(edit, line, todo.output())
            else:
                continue

    def is_visible(self):
        self.todotxt_file_path = path_from_settings()
        if self.todotxt_file_path is None or os.path.exists(self.todotxt_file_path) is False:
            return False
        elif self.view.file_name() is not None and os.path.samefile(self.view.file_name(), self.todotxt_file_path):
            return True
        else:
            return False


class TodoCompletionListInputHandler(sublime_plugin.ListInputHandler):
    def __init__(self, view, todo_items):
        self.view = view
        self.todo_items = todo_items

    def placeholder(self):
        return "Select todo to complete."

    def list_items(self):
        return [item.text for item in self.todo_items if not item.completed]


class TodoTxtCompleteTodoInteractiveCommand(sublime_plugin.TextCommand):
    def run(self, edit, todo_completion_list):
        # If the file is not open I do want to pull focus to it which the
        # open_file command does automatically.
        todo_view = self.view.window().open_file(self.todotxt_file_path)
        if todo_view.is_loading():
            global targetView, pending_output, event
            event = 'complete'
            targetView = todo_view
            pending_output = todo_completion_list
        else:
            todo_view.run_command('todo_txt_move_to_todo', {'todo_item':todo_completion_list})

    def input(self, args):
        self.todotxt_file_path = path_from_settings()
        if self.todotxt_file_path is None or os.path.exists(self.todotxt_file_path) is False:
            # TODO: error out here as we do not have a file set
            return None
        else: 
            self.todo_items = []

            todo_view = self.view.window().find_open_file(self.todotxt_file_path)
            if todo_view:
                region = sublime.Region(0, todo_view.size())
                contents = todo_view.substr(region)
                for line in contents.splitlines():
                    todo = TodoTxtItem()
                    todo.parse(line)
                    self.todo_items.append(todo)
            else:
                with open(self.todotxt_file_path) as todofile:
                    for line in todofile:
                        todo = TodoTxtItem()
                        todo.parse(line)
                        self.todo_items.append(todo)
            return TodoCompletionListInputHandler(self.view, self.todo_items)

    def is_visible(self):
        self.todotxt_file_path = path_from_settings()
        if self.todotxt_file_path is None or os.path.exists(self.todotxt_file_path) is False:
            return False
        else:
            return True


class TodoTxtMoveToTodoCommand(sublime_plugin.TextCommand):
    def run(self, edit, todo_item):
        region = self.view.find(re.escape(todo_item), 0)
        selection = self.view.sel()
        selection.clear()
        selection.add(region)
        self.view.run_command('todo_txt_complete_selected_todo')
        row = self.view.rowcol(region.begin())[0]
        selection.clear()
        selection.add(self.view.text_point(row,0))
